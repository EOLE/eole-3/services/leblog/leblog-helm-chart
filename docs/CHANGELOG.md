# Changelog

## [1.8.0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/leblog/leblog-helm-chart/compare/release/1.7.1...release/1.8.0) (2025-03-03)


### Features

* update appVersion to 1.11.2 ([daf71ad](https://gitlab.mim-libre.fr/EOLE/eole-3/services/leblog/leblog-helm-chart/commit/daf71ad916de8fd4b1d02a83a4c3a1a3d1a83d74))
* update appVersion to testing ([fd26970](https://gitlab.mim-libre.fr/EOLE/eole-3/services/leblog/leblog-helm-chart/commit/fd269708bb7f516a16b13e5fc0c5ede15cecce03))

### [1.7.1](https://gitlab.mim-libre.fr/EOLE/eole-3/services/leblog/leblog-helm-chart/compare/release/1.7.0...release/1.7.1) (2024-10-07)


### Bug Fixes

* deploy appVersion to 1.11.1 ([2eb340f](https://gitlab.mim-libre.fr/EOLE/eole-3/services/leblog/leblog-helm-chart/commit/2eb340fa514d3a712e0785def8104e823233f8c0))

## [1.7.0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/leblog/leblog-helm-chart/compare/release/1.6.0...release/1.7.0) (2024-06-12)


### Features

* update appVersion to 1.6.3 for api and 1.11.0 for front ([3029171](https://gitlab.mim-libre.fr/EOLE/eole-3/services/leblog/leblog-helm-chart/commit/3029171eaa1f4df9cd23e79105282cf716a542c7))


### Bug Fixes

* helm dev version deploy dev image tag ([1d6cb18](https://gitlab.mim-libre.fr/EOLE/eole-3/services/leblog/leblog-helm-chart/commit/1d6cb1886316db79466d29b59721892875f62247))
* restore testing tag ([93a7dbf](https://gitlab.mim-libre.fr/EOLE/eole-3/services/leblog/leblog-helm-chart/commit/93a7dbfbc566dffc56cb76a3f7116c9b0d54a872))
* restore testing tag ([e5a792e](https://gitlab.mim-libre.fr/EOLE/eole-3/services/leblog/leblog-helm-chart/commit/e5a792e79d393142294fd771923638b975d51293))
* wait 5 minutes before killing pod ([f684e82](https://gitlab.mim-libre.fr/EOLE/eole-3/services/leblog/leblog-helm-chart/commit/f684e82dd2ebece7d323c48a84b17283203aa8d2))

## [1.6.0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/leblog/leblog-helm-chart/compare/release/1.5.0...release/1.6.0) (2024-01-16)


### Features

* update blog_front to 1.10.0 and blog_api to 1.6.2 ([e864e39](https://gitlab.mim-libre.fr/EOLE/eole-3/services/leblog/leblog-helm-chart/commit/e864e399b8fcabd8da54534fb4975273af15864a))

## [1.5.0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/leblog/leblog-helm-chart/compare/release/1.4.0...release/1.5.0) (2023-11-07)


### Features

* new blogapi version 1.6.1 ([6d8eb37](https://gitlab.mim-libre.fr/EOLE/eole-3/services/leblog/leblog-helm-chart/commit/6d8eb37dae8070310bf052292210fe22cd932e6c))

## [1.4.0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/leblog/leblog-helm-chart/compare/release/1.3.1...release/1.4.0) (2023-10-17)


### Features

* new stable chart for blog-front version 1.9.0 ([8aba1b4](https://gitlab.mim-libre.fr/EOLE/eole-3/services/leblog/leblog-helm-chart/commit/8aba1b4a7623536a48661df004e604b774987156))


### Bug Fixes

* appversion reference testing image ([e0629f5](https://gitlab.mim-libre.fr/EOLE/eole-3/services/leblog/leblog-helm-chart/commit/e0629f5891f9f4d0983f69469cc5b16dad037611))

### [1.3.1](https://gitlab.mim-libre.fr/EOLE/eole-3/services/leblog/leblog-helm-chart/compare/release/1.3.0...release/1.3.1) (2023-10-02)


### Bug Fixes

* update to blog front 1.8.1 ([d4ad875](https://gitlab.mim-libre.fr/EOLE/eole-3/services/leblog/leblog-helm-chart/commit/d4ad8751575c33b3c9518a1341ab5b212f1878a2))

## [1.3.0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/leblog/leblog-helm-chart/compare/release/1.2.1...release/1.3.0) (2023-08-25)


### Features

* new chart version for blog 1.8.0 and blogapi 1.6.0 ([2e064d1](https://gitlab.mim-libre.fr/EOLE/eole-3/services/leblog/leblog-helm-chart/commit/2e064d1135d72ce2facd60f15832da8005d516a5))

### [1.2.1](https://gitlab.mim-libre.fr/EOLE/eole-3/services/leblog/leblog-helm-chart/compare/release/1.2.0...release/1.2.1) (2023-05-09)


### Bug Fixes

* publish new helm stable version ([5bf4bc1](https://gitlab.mim-libre.fr/EOLE/eole-3/services/leblog/leblog-helm-chart/commit/5bf4bc1b05be934cc4ba5fe414f4267b0f0711bf))
* reorder chart.yaml ([06fc3cf](https://gitlab.mim-libre.fr/EOLE/eole-3/services/leblog/leblog-helm-chart/commit/06fc3cfe074cb9f97c8801e38947d3502c1b4570))
* update readme ([dd106b6](https://gitlab.mim-libre.fr/EOLE/eole-3/services/leblog/leblog-helm-chart/commit/dd106b6a148bb70a6c6a6db09705c65fc51a6f81))

## [1.2.0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/leblog/leblog-helm-chart/compare/release/1.1.0...release/1.2.0) (2023-04-18)


### Features

* publish stable version for blog front 1.7.0 ([2a7c58d](https://gitlab.mim-libre.fr/EOLE/eole-3/services/leblog/leblog-helm-chart/commit/2a7c58dddae8f89b9bdf1347671bf5ae15879165))

## [1.1.0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/leblog/compare/release/1.0.0...release/1.1.0) (2023-02-01)


### Features

* publish blog and blogapi 1.5.0 ([6b771f3](https://gitlab.mim-libre.fr/EOLE/eole-3/services/leblog/commit/6b771f31e2177c663411b84f5f7fa9732f0b112f))


### Bug Fixes

* blogapi subchart have a dev version ([2dd4405](https://gitlab.mim-libre.fr/EOLE/eole-3/services/leblog/commit/2dd4405940eb796d84dc0ac5f4cc6c32d09f1755))
* dev chart version deploy dev app version ([b070b0e](https://gitlab.mim-libre.fr/EOLE/eole-3/services/leblog/commit/b070b0ee28ba486bdb79daca9c7a9ae1e0b352d8))
* testing helm deploy testing app version ([119a9e7](https://gitlab.mim-libre.fr/EOLE/eole-3/services/leblog/commit/119a9e70065a968acd85232b1f8fdedd0e91fce1))

## 1.0.0 (2022-12-09)


### Features

* **ci:** build helm package ([e9bcb15](https://gitlab.mim-libre.fr/EOLE/eole-3/services/leblog/commit/e9bcb1596cccfb1f157bf7528e7605ca3d62d356))
* **ci:** push helm package to chart repository ([914d2b1](https://gitlab.mim-libre.fr/EOLE/eole-3/services/leblog/commit/914d2b1b705bbe0e2df64d34e8425e60fd23764e))
* **ci:** update helm chart version with `semantic-release` ([75a6807](https://gitlab.mim-libre.fr/EOLE/eole-3/services/leblog/commit/75a680784aa94f75e26d58edb1b325429f8b3f28))
* **ci:** validate helm chart at `lint` stage ([29092e9](https://gitlab.mim-libre.fr/EOLE/eole-3/services/leblog/commit/29092e91a4cbdfa320d471b0848526ad5db81837))
* update app version and dependency ([d238f9b](https://gitlab.mim-libre.fr/EOLE/eole-3/services/leblog/commit/d238f9b6fcd924d94dc38419db2a29e12ec64d53))
* use chart.yaml as default version ([ffcff72](https://gitlab.mim-libre.fr/EOLE/eole-3/services/leblog/commit/ffcff724db853d23f6f1583717fada5aa057cdf3))
* use chart.yaml as default version ([27d73df](https://gitlab.mim-libre.fr/EOLE/eole-3/services/leblog/commit/27d73df735535a9826ffec257745451279107e99))


### Bug Fixes

* **leblog:** add containerport default values for leblog and blogapi ([1333d88](https://gitlab.mim-libre.fr/EOLE/eole-3/services/leblog/commit/1333d88ec7a42ece409518147bd2188881fc7453))
* **leblog:** add image repository default values for leblog and blogapi ([5a388cf](https://gitlab.mim-libre.fr/EOLE/eole-3/services/leblog/commit/5a388cff58b53714d03f036f03dc431f830fc0fa))
* update hpa api version removed on k8s 1.26 ([c46dde1](https://gitlab.mim-libre.fr/EOLE/eole-3/services/leblog/commit/c46dde19a664c28b7cdc2835d8be5ef0e3d2b15b))


### Continuous Integration

* **commitlint:** enforce commit message format ([b7225ae](https://gitlab.mim-libre.fr/EOLE/eole-3/services/leblog/commit/b7225ae4c009afd3dc219b2c048c1f7b4e1b32f5))
* **release:** create release automatically with `semantic-release` ([6ef234b](https://gitlab.mim-libre.fr/EOLE/eole-3/services/leblog/commit/6ef234ba524d53f06081ee0ac24567d2d5ac124a))
